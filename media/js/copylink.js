/**
 * @package n3t Copylink
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2022 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

document.addEventListener("copy", (e) => {
  const options = Joomla.getOptions('plg_n3tcopylink'),
    helper = document.createElement("div");

  // prepare suffixes
  let suffix = options.suffix,
    suffixHtml = options.suffixHtml;

  suffix = suffix.replace('{url}', document.location.href);
  suffix = suffix.replace('{title}', document.title);

  suffixHtml = suffixHtml.replace('{url}', document.location.href);
  suffixHtml = suffixHtml.replace('{title}', document.title);

  // get clipboard contents
  helper.appendChild(window.getSelection().getRangeAt(0).cloneContents());
  let text = document.getSelection().toString(),
    html = helper.innerHTML;
  helper.remove();

  // do not copylink small selections
  if (text.length < options.min_selection)
    return;

  // set clipboard data
  e.clipboardData.setData("text/plain", `${text}\n${suffix}`);
  e.clipboardData.setData("text/html", `${html}${suffixHtml}`);
  e.preventDefault();
});