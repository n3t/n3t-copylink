<?php
/**
 * @package n3t Copylink
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2022 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Version;

class plgSystemN3tCopylink extends CMSPlugin {

  public function onAfterDispatch()
  {    
    $app = Factory::getApplication();
	  $doc = $app->getDocument();

		if (!$app->isClient('site'))
			return;

	  if ($doc->getType() !== 'html')
		  return;

	  $this->loadLanguage();

	  if (Version::MAJOR_VERSION >= 4) {
		  $doc->getWebAssetManager()->getRegistry()->addExtensionRegistryFile('plg_n3tcopylink');
		  $doc->getWebAssetManager()->useScript('plg_n3tcopylink.copylink');
	  } else
	    HTMLHelper::script('plg_n3tcopylink/copylink.min.js', ['version' => 'auto', 'relative' => true], ['defer' => true]);

	  $suffix = $this->params->get('suffix', Text::_('PLG_SYSTEM_N3TCOPYLINK_DEFAULT_SUFFIX'));
	  $suffix = str_ireplace( '{siteurl}', htmlspecialchars(Uri::base(false)), $suffix);
	  if (Version::MAJOR_VERSION >= 4)
	    $suffix = str_ireplace('{sitetitle}', htmlspecialchars($app->get('sitename')), $suffix);
		else
			$suffix = str_ireplace('{sitetitle}', htmlspecialchars($app->getCfg('sitename')), $suffix);

	  $doc->addScriptOptions('plg_n3tcopylink', [
		  'min_selection' => (int)$this->params->get('min_selection', 20),
		  'suffix' => strip_tags($suffix),
		  'suffixHtml' => $suffix,
	  ]);
  }
}
