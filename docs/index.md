n3t Copylink
============

n3t Copylink is system plugin enabling to specify suffix to any text copied from the website.

Installation
------------

n3t Copylink is Joomla! system plugin. It could be installed as any other extension in 
Joomla!

After installing __do not forget to enable the plugin__ from Plugin Manager in your
Joomla! installation.
