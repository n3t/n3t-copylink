Settings
========

Available settings in plugin manager are:

#### Custom suffix

Here you can specify custom text instead of standard one. The standard text looks like 
"Source: http://www.example.com/mypage". 

There are some tags that could be used in the custom suffix:

- `{url}` is replaced by current page address.
- `{title}` is replaced by current page title.
- `{siteurl}` is replaced by home page address.
- `{siteurl}` is replaced by title, as is set in global configuration.

#### Minimal text length

Specifies minimal length of the copied text, for which the source information appendix will be applied. 
If the value is 1, it will be applied to any content copied from the site. The standard value is 20.