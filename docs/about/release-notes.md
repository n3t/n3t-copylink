Release notes
=============

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[4.1.x]
-------

### [4.1.0] - 2022-07-01

#### Changed
- completely rewritten JavaScript solution
- Improved installer
- Joomla 4 installer compatibility
- Minimal Joomla 3.10 and PHP 7.2 required

[4.0.x]
-------

### [4.0.0] - 2019-04-19

#### Added
- Joomla 4 compatibility

[3.0.x]
-------

### [3.0.2] - 2017-03-05

#### Changed
- New Update server
- Improved installer

### [3.0.1] - 2016-01-17

#### Changed
- New Update server

### [3.0.0] - 2015-12-30

#### Added
- Joomla 2.5 support discontinued, Joomla 3.4 only and later is supported now
- {siteurl}, {title} and {sitetitle} tag support
- New Help site
#### Changed
- Code cleanup
- Translations are not included in basic installation anymore

[1.0.x]
-------
  
### [1.0.2] - 2012-12-06

#### Changed
- PHP Strict standards changes
#### Added
- it-IT translation - thanks to Fabio Perri at Transifex

### [1.0.1] - 2012-11-06

#### Fixed
- Single quote slashing missing
#### Changed
- Mootools are not explicitly loaded anymore

### [1.0.0] - 2012-11-05

- Initial release