n3t Copylink
============

[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-v3.10-green)][JOOMLA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-v4.x-green)][JOOMLA]
[![PHP](https://img.shields.io/badge/PHP-v7.2-green)][PHP]
[![Documentation Status](https://readthedocs.org/projects/n3t-copylink/badge/?version=latest)][DOCS]

n3t Copylink is system plugin enabling to specify suffix to any text copied from the website.

Installation
------------

n3t Copylink is Joomla! system plugin. It could be installed as any other extension in 
Joomla!

After installing **do not forget to enable the plugin** from Plugin Manager in your
Joomla! installation.

Usage
-----

There is no special usage of this plugin. It will start to work automatically, once it is installed and enabled in Plugin manager.

Documentation
-------------

Detailed documentation could be found at [n3t Copylink documentation page][docs]

[JOOMLA]: https://www.joomla.org
[PHP]: https://www.php.net
[DOCS]: http://n3tcopylink.docs.n3t.cz/en/latest/